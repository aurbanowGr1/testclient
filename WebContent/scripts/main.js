AddressViewModel = function()
{
	var self = this;
	self.city=ko.observable();
	self.country=ko.observable();
	self.street=ko.observable();
	self.houseNumber=ko.observable();
	self.localNumber=ko.observable();
	self.postalCode=ko.observable();
}

PersonViewModel = function(){
	var self = this;
	self.firstName=ko.observable("unknown");
	self.surname=ko.observable("unknown");
	
	self.addresses=ko.observableArray([]);
	
	self.addAddress = function()
	{
		var address = new AddressViewModel();
		self.addresses.push(address);
	}
	
	self.save = function()
	{
		 var data = ko.toJSON(self);
	        $.ajax({
	            url: "http://localhost:8080/TestProject/resources/persons/add",
	            type: "POST",
	            data: data,
	            contentType: "application/json",
	            success: function (data) {
	                alert(data);
	            },
	            error: function (XMLHttpRequest, testStatus, errorThrown) {
	                if (XMLHttpRequest.status === 400) {
	                    $('#error').text(XMLHttpRequest.responseText)
	                }
	                else {
	                    $('#error').text("nieoczekiwany błąd.")
	                }

	            }
	        });
	}
}

function init(){
	$("form").validate({
	    submitHandler: function (){
	        viewModel.save();
	    }, 
	    rules:{
	    	firstname:{
	    		required:true
	    	}
	    }
	});
}